/*jshint esversion: 8 */
/**
* @author Rafik BOUCHENNA et Axel RAIMONDO
* @copyright Rafik and Axel
*/
//declaration 
const main = document.getElementById('poster');
const aside = document.querySelector('aside');
const pHeader = document.querySelector('header p');
const pFooter = document.querySelector('footer p');
const h1Aside = document.querySelector('aside h1');
const article = document.querySelector('article');
const h1Article = document.querySelector('article h1');
const pArticle = document.querySelector('article p');
const header = document.querySelector('header');
const footer = document.querySelector('footer');
//appel des fonctions de base 
majCSS();
initialiserImage();
deposerImageSelectionner();


// Fonction pour créer image
function initialiserImage(){
  //6 images
  for(let i=0; i<6; i++){
    const zone = document.createElement('div');
    
    zone.classList.add("depot");
    
    main.appendChild(zone);
    
    const image = document.createElement('img');
    //utilisation de l'api unspash -> l'api pexels marche tres bien  mais on prefere utiliser unsplash pour eviter de pagnier 
    fetch("https://api.unsplash.com/photos/random/?client_id=79_FbCF5IRPLe1LK2OGNnOCCBrW0YVM9OmSXgPAmYqw")
    .then(function(response) {
      return response.json();
    })
    .then(function(data) {
      image.setAttribute("src",data['urls']['full']+".jpg");//ajout du lien de l'image
      console.log(data['urls']['full']);
    })
    .catch(function(error) {
      console.log("Une erreur s'est produite:", error);
    });
    //style de l'image
    image.style.width = '100px';
    image.style.height = '100px';
    image.style.cursor = 'move';
    image.id = "img"+(i+1);
    image.deplacement = true;
    aside.appendChild(image);
  }
  
}

const zones = document.querySelectorAll('img, .depot');

function deselectionSauf(zoneClique) {
  zones.forEach(zone => {
    if (zone !== zoneClique) {
      zone.classList.remove('selected');
    }
  });
}

function borderImage(event) {
  
  
  
  const zoneClique = event.target;
  
  console.log(zoneClique);
  
  deselectionSauf(zoneClique);
  
  
  const aLaClasseSelected = zoneClique.classList.contains('selected');
  
  
  if (!aLaClasseSelected) {
    zoneClique.classList.add('selected');
  } else {
    zoneClique.classList.remove('selected');
  }
}


zones.forEach(zone => {
  zone.addEventListener('click', borderImage);
});

let id = "";


document.addEventListener('DOMContentLoaded', function() {
  const deplacements = document.querySelectorAll('img');
  const posements = document.querySelectorAll('.depot');
  
  deplacements.forEach(deplacement => {
    deplacement.addEventListener('dragstart', (event) => {
      id = deplacement.id;
      event.dataTransfer.setData('text/plain', deplacement.id);
    });
  });
  
  posements.forEach(posement => {
    posement.addEventListener('dragover', (event) => {
      event.preventDefault();
    });
    
    posement.addEventListener('drop', (event) => {
      event.preventDefault();
      const draggedElement = document.getElementById(id);
      if(posement.children.length==0){
        //verifie si il y a deja une image dans la zone 
        posement.appendChild(draggedElement);
      }else{
        alert("Vous avez déja une image dans ce dépôt"); 
      }
    });
  });
});

function deposerImageSelectionner(){
  const listeImages  = aside.children;
  const listeDepot = main.children;
  
  Array.from(listeImages).forEach(img => {
    img.addEventListener('click', function() {
      Array.from(listeDepot).forEach(depot => {
        if (depot.classList.contains('selected')) {
          console.log("le parent "+depot.children.length);
          if(depot.children.length==0){
            //verifie si il y a deja une image dans la zone 
            depot.appendChild(img);
          }else{
            alert("Vous avez déja une image dans ce dépôt"); 
          }
          
        }
      });
    });
  });
}

document.addEventListener('DOMContentLoaded', function() {
  const img = document.querySelectorAll('img');
  const aside = document.querySelector('aside');
  
  img.forEach(deplacement => {
    deplacement.addEventListener('dragstart', (event) => {
      //deplacement de l'image
      id = deplacement.id;
      event.dataTransfer.setData('text/plain', deplacement.id);
    });
  });
  
  
  aside.addEventListener('dragover', (event) => {
    event.preventDefault();
    
    
    aside.addEventListener('drop', (event) => {
      event.preventDefault();
      const draggedElement = document.getElementById(id);
      aside.appendChild(draggedElement);
    });
  });
});




function majCSS(){
  //mise en forme de la feuille de style en js 
  const titreSite = document.createElement('h1');
  const titreFooter = document.createElement('h1');
  pHeader.parentNode.replaceChild(titreSite, pHeader);
  
  titreSite.textContent = "MosaicMaster Pro";
  titreSite.style.color = 'white';
  
  pFooter.parentNode.replaceChild(titreFooter, pFooter);
  titreFooter.textContent = "Rafik BOUCHENNA && Axel RAIMONDO"
  titreFooter.style.color = 'white';
  
  
  aside.style.background = '#0276FF';
  aside.style.width = "250px";
  aside.style.color = "white";
  h1Aside.style.color = 'white';
  
  main.style.background = 'rgb(205, 232, 255)';
  
  h1Article.style.color = 'white';
  pArticle.style.color = 'white';
  article.style.background = '#0276FF';
  article.style.width = "200px";
  document.body.style.background = 'aliceblue';
  document.body.style.width = '90%';
  let input = document.createElement('input');
  input.setAttribute('type',"number");
  article.appendChild(input);
  let btn = document.createElement('input');
  btn.setAttribute("id","btn");
  
  btn.style.height = "30px";
  btn.style.width = "100px";
  btn.setAttribute("type","submit");
  btn.setAttribute("value","Terminer");
  article.appendChild(btn);
  btn.addEventListener("click",function(){
    document.querySelectorAll('.depot').forEach(elt =>{
      elt.style.height= input.value +"px";
      elt.style.width= input.value +"px";
      const images = document.querySelectorAll('img');
      images.forEach(image => {
        image.style.width = '100%'; // Vous pouvez ajuster cela selon vos besoins
        image.style.height = '100%'; // Vous pouvez ajuster cela selon vos besoins
      });
    })
    // document.querySelectorAll('.depot')[0].style.width= input.value+"px";
  });
  let inputColor = document.createElement('input');
  inputColor.setAttribute('type','color');
  article.appendChild(inputColor);
  inputColor.addEventListener("click",function(){
    main.style.backgroundColor = inputColor.value;
  })
  let paragrapheElement = document.querySelector('article p');
  paragrapheElement.innerHTML = " Cliquer sur une zone vide puis sur une photo pour l'ajouter au pêle-mêle,Vous pouvez reajustez la taille des photos SANS EXAGERER et vous pouvez changer de color en cliquant sur l'input color"
  header.style.border = 'solid white 1px';
  header.style.background = '#0276FF';
  header.style.borderRadius ="10px";
  footer.style.border = 'solid white 1px';
  footer.style.background = '#0276FF';
  footer.style.borderRadius ="10px";
  
  
}
